-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 2021-05-06 22:54:13
-- 服务器版本： 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `leaf_ordersystem`
--

-- --------------------------------------------------------

--
-- 替换视图以便查看 `cc`
--
CREATE TABLE IF NOT EXISTS `cc` (
`id` int(11)
,`name` varchar(11)
,`price` double
,`flavor` varchar(11)
,`mid` int(11)
);
-- --------------------------------------------------------

--
-- 视图结构 `cc`
--
DROP TABLE IF EXISTS `cc`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `cc` AS select `leaf_menu`.`id` AS `id`,`leaf_menu`.`name` AS `name`,`leaf_menu`.`price` AS `price`,`leaf_menu`.`flavor` AS `flavor`,`leaf_menu`.`mid` AS `mid` from `leaf_menu`;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

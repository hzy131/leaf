package com.night.feign;

import com.night.entity.Role;
import com.night.entity.User;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@FeignClient(value = "account")
public interface AccountFeign {
    @GetMapping("/account/login/{username}/{password}")
    User login(@PathVariable("username") String username, @PathVariable("password") String password);

    //    @RequestMapping("redis/hasKey/{key}")
//    public Boolean hasKey(@PathVariable("key") String key);
    @GetMapping("/account/findByUserName/{username}")
    User findByUserName(@PathVariable("username") String username);

    @GetMapping("/account/findByRoleName/{username}")
    List<Role> findByRoleName(@PathVariable("username") String username);

    @GetMapping("/account/findRoleAll")
    List<Role> findRoleAll();
}

package com.night.feign;


import com.night.entity.Menu;
import com.night.entity.MenuVo;
import com.night.entity.Type;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@FeignClient(value = "menu")
public interface MenuFeign {

    @GetMapping("/menu/findAll/{page}/{limit}")
    MenuVo findAll(@PathVariable("page") int page, @PathVariable("limit") int limit);

    @GetMapping("/menu/typeFindAll")
    List<Type> TypeFindAll();

    @PostMapping("/menu/update")
    void update(@RequestBody Menu menu);

    @GetMapping("/menu/deleteById/{id}")
    void deleteById(@PathVariable("id") Long id);

    @PostMapping("/menu/save")
    void save(@RequestBody Menu menu);

    @GetMapping("/menu/findById/{id}")
    Menu findById(@PathVariable("id") Long id);
}

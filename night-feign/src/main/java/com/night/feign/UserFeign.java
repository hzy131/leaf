package com.night.feign;

import com.night.entity.User;
import com.night.entity.UserVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

@FeignClient(value = "user")
public interface UserFeign {
    @GetMapping("/user/findAll")
    UserVo findAll();

    @GetMapping("/user/findById/{id}")
    User findById(@PathVariable("id") Long id);

    @PostMapping("/user/save")
    void save(@RequestBody User user);

    @PostMapping("/user/update")
    void update(@RequestBody User user);

    @GetMapping("/user/deleteById/{id}")
    void deleteById(@PathVariable("id") Long id);
    @PostMapping("/user/updatePassword")
    void updatePassword(@RequestBody User user);
}

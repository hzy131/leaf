package com.night.feign;

import com.night.entity.Order;
import com.night.entity.OrderVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

@FeignClient(value = "order")
public interface OrderFeign {

    @PostMapping("/order/save")
    void save(@RequestBody Order order);

    @GetMapping("/order/findAll/{page}/{limit}/{state}")
    OrderVO findAllByUid(@PathVariable("page") int page, @PathVariable("limit") int limit, @PathVariable("state") int state);

    //
    @DeleteMapping("/order/deleteByMid/{mid}")
    void deleteByMid(@PathVariable("mid") long mid);

    @DeleteMapping("/order/deleteByUid/{uid}")
    void deleteByUid(@PathVariable("uid") long uid);

    //
    @GetMapping("/order/findAllByState/{state}/{page}/{limit}")
    OrderVO findAllByState(@PathVariable("state") int state, @PathVariable("page") int page, @PathVariable("limit") int limit);

    //
    @RequestMapping("/order//updateState/{id}")
    public void updateState(@PathVariable("id") long id);
}

package com.night.service;

import com.night.entity.Role;
import com.night.entity.User;
import com.night.feign.AccountFeign;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@Slf4j
public class UserDetailService implements UserDetailsService {
    @Autowired
    private AccountFeign accountFeign;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User userName = accountFeign.findByUserName(username);
        if (userName == null) {
            return null;
        }
        List<Role> byRoleName = accountFeign.findByRoleName(username);
        List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        byRoleName.forEach((Role) -> {
            authorities.add(new SimpleGrantedAuthority(Role.getRoleName()));
            log.info(">>>authorities:{}<<<", authorities);
        });
        userName.setAuthorities(authorities);
        return userName;
    }
}

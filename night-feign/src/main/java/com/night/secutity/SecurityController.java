package com.night.secutity;


import com.night.entity.Role;
import com.night.feign.AccountFeign;
import com.night.filter.MD5Util;
import com.night.service.UserDetailService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.List;

@EnableWebSecurity
@Component
@Slf4j
public class SecurityController extends WebSecurityConfigurerAdapter {
    @Autowired
    private UserDetailService userDetailService;

    @Autowired
    private AccountFeign accountFeign;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        List<Role> roleAll = accountFeign.findRoleAll();
        ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry
                expressionInterceptUrlRegistry = http.authorizeRequests();
        roleAll.forEach((userDetailService) -> {
            expressionInterceptUrlRegistry
                    .antMatchers("/menu/redirect/main").hasAnyAuthority(userDetailService.getRoleName());
            log.info(">>>authorities:{}<<<", userDetailService.getRoleName());
        });
        //禁用跨站csrf攻击防御
        expressionInterceptUrlRegistry
                .antMatchers("/login").permitAll()
                .antMatchers("/**").fullyAuthenticated()
                .and()
                .formLogin()
                .loginPage("/login")
                .defaultSuccessUrl("/tologin")
//                .successForwardUrl("/menu/redirect/main")
//                .successHandler(new LoginSuccessHandler())
                .and().csrf().disable().headers().frameOptions().sameOrigin()
        ;
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers("/resources/**", "/static/**", "/layui/**", "/images/**", "/js/**");
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailService).passwordEncoder(new PasswordEncoder() {
            @Override
            public String encode(CharSequence charSequence) {
                return MD5Util.encode((String) charSequence);
            }

            @Override
            public boolean matches(CharSequence charSequence, String s) {
                String rawPass = MD5Util.encode((String) charSequence);
                boolean result = rawPass.equals(s);
                return result;
            }
        });
    }
}
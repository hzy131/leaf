package com.night.controller;

import com.night.entity.User;
import com.night.entity.UserVo;
import com.night.feign.AccountFeign;
import com.night.feign.UserFeign;
import com.night.filter.MD5Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.Date;

@Controller
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserFeign userFeign;

    @Autowired
    private AccountFeign accountFeign;

    @GetMapping("/findAll")
    @ResponseBody
    public UserVo findAll() {
        return userFeign.findAll();

    }

    @GetMapping("/findById/{id}")
    public String findById(@PathVariable("id") long id) {
        userFeign.findById(id);
        return "redirect:/menu/redirect/user_manage";
    }

    @GetMapping("/findByName")
    public String findByName(Model model, HttpSession session) {
        String name = (String) session.getAttribute("name");
        User user = accountFeign.findByUserName(name);
        model.addAttribute("user", user);
        return "account_update";
    }

    @PostMapping("/save")
    public String save(User user) {
        user.setRegisterDate(new Date());
        String encode = MD5Util.encode(user.getPassword());
        user.setPassword(encode);
        userFeign.save(user);
        return "redirect:/menu/redirect/user_manage";
    }

    @GetMapping("/deleteById/{id}")
    public String deleteById(@PathVariable("id") Long id) {
        userFeign.deleteById(id);
        return "redirect:/menu/redirect/user_manage";
    }

    @RequestMapping("/update")
    public String update(User user) {
        System.out.println(user);
        user.setRegisterDate(new Date());
        userFeign.update(user);
        return "redirect:/menu/redirect/user_manage";
    }
    @RequestMapping("/updatePassword")
    public String updatePassword(User user,HttpSession httpSession) {
        String name = (String) httpSession.getAttribute("name");
        user.setUsername(name);
        user.setPassword(MD5Util.encode(user.getPassword()));
        System.out.println(user);
        userFeign.updatePassword(user);
        httpSession.invalidate();
        return "login.html";
    }

}

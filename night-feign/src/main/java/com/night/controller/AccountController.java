package com.night.controller;

import com.night.entity.Role;
import com.night.entity.User;
import com.night.feign.AccountFeign;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
@Slf4j
public class AccountController {
    @Autowired
    private AccountFeign accountFeign;
//    @Autowired
//    public StringRedisTemplate stringRedisTemplate;

    /**
     * 判断key是否存在
     */
//    @RequestMapping("/redis/hasKey/{key}")
//    public Boolean hasKey(@PathVariable("key") String key) {
//        try {
//            return stringRedisTemplate.hasKey(key);
//        } catch (Exception e) {
//            e.printStackTrace();
//            return false;
//        }
//    }

    /**
     * //     * 校验用户名密码，成功则返回通行令牌
     * //
     */
//    @RequestMapping("/checkUsernameAndPassword")
//    public String checkUsernameAndPassword(String username, String password) {
//        //通行令牌
//        String flag = null;
//        User account = accountFeign.login(username, password);
//        if (!StringUtils.isEmpty(account)) {
//            //用户名+时间戳（这里只是demo，正常项目的令牌应该要更为复杂）
//            flag = username + System.currentTimeMillis();
//            //令牌作为key，存用户id作为value（或者直接存储可暴露的部分用户信息也行）设置过期时间（我这里设置3分钟）
//            stringRedisTemplate.opsForValue().set(flag, "1", (long) (3 * 60), TimeUnit.SECONDS);
//        }
//        return flag;
//    }

    /**
     * 跳转登录页面
     */
    @GetMapping("/login")
    public String login() {
        return "login";
    }

    @RequestMapping("/tologin")
    public String tologin(HttpSession session) {
        String name = SecurityContextHolder.getContext().getAuthentication().getName();
        log.info("用户" + name);
        session.setAttribute("name", name);
        return "main";
    }

    /**
     * 注销
     */
    @GetMapping("/logout")
    public String logout(HttpSession httpSession) {
        httpSession.invalidate();
        return "redirect:/menu/redirect/login";
    }

    @GetMapping("/findByUserName")
    public User findByUserName(@RequestParam("username") String username) {
        return accountFeign.findByUserName(username);
    }

    @GetMapping("/findByRoleName")
    public List<Role> findByRoleName(@RequestParam("username") String username) {
        return accountFeign.findByRoleName(username);
    }
}

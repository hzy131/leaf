package com.night.controller;

import com.night.entity.Menu;
import com.night.entity.Order;
import com.night.entity.OrderVO;
import com.night.entity.User;
import com.night.feign.AccountFeign;
import com.night.feign.OrderFeign;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.Date;

@Controller
@RequestMapping("/order")
public class OrderController {
    @Autowired
    private OrderFeign orderFeign;
    @Autowired
    private AccountFeign userFeign;

    @GetMapping("/save/{mid}")
    public String save(@PathVariable("mid") int mid, HttpSession Session) {
        String name = (String) Session.getAttribute("name");
        User user = new User();
        User user1 = userFeign.findByUserName(name);
        user.setId(user1.getId());
        Order order = new Order();
        Menu menu = new Menu();
        menu.setId(mid);
        order.setUser(user);
        order.setMenu(menu);
        order.setDate(new Date());
        orderFeign.save(order);
        return "order_handler";
    }

    @GetMapping("/index")
    public String index() {
        return "index";
    }

    @GetMapping("/findAll_UnDelivered")
    @ResponseBody
    public OrderVO findAll_UnDelivered(@RequestParam("page") int page, @RequestParam("limit") int limit) {
        return orderFeign.findAllByUid(page, limit, 0);
    }

    @GetMapping("/findAll_Delivered")
    @ResponseBody
    public OrderVO findAll_Delivered(@RequestParam("page") int page, @RequestParam("limit") int limit) {
        return orderFeign.findAllByUid(page, limit, 1);
    }

    @GetMapping("/findAllByState")
    @ResponseBody
    public OrderVO findAllByState(@RequestParam("page") int page, @RequestParam("limit") int limit) {
        return orderFeign.findAllByState(0, page, limit);
    }

    @RequestMapping("/updateState/{id}")
    public String updateState(@PathVariable("id") long id) {
        System.out.println(id);
        orderFeign.updateState(id);
        return "order_handler";
    }
}

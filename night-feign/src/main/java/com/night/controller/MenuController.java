package com.night.controller;

import com.night.entity.Menu;
import com.night.entity.MenuVo;
import com.night.feign.MenuFeign;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/menu")
@Slf4j
public class MenuController {
    @Autowired
    private MenuFeign menuFeign;

    @GetMapping("/findAll")
    @ResponseBody
    public MenuVo findAll(@RequestParam("page") int page, @RequestParam("limit") int limit) {
        return menuFeign.findAll(page, limit);
    }

    @RequestMapping("/redirect/{location}")
    public String redirect(@PathVariable("location") String location, Model model) {
        model.addAttribute("list", menuFeign.TypeFindAll());
        return location;
    }

    @GetMapping("/deleteById/{id}")
    public String deleteById(@PathVariable("id") Long id) {
        menuFeign.deleteById(id);
        return "redirect:/menu/redirect/menu_manage";
    }

    @PostMapping("/save")
    public String save(Menu menu) {
        menuFeign.save(menu);
        return "redirect:/menu/redirect/menu_manage";
    }

    @PostMapping("/update")
    public String update(Menu menu) {
        menuFeign.update(menu);
        return "redirect:/menu/redirect/menu_manage";
    }

    @GetMapping("/findById/{id}")
    public String findById(@PathVariable("id") Long id, Model model) {
        model.addAttribute("menu", menuFeign.findById(id));
        model.addAttribute("list", menuFeign.TypeFindAll());
        log.info("修改");
        return "menu_update";
    }
}

package com.night.service;


import com.night.entity.User;

import java.util.List;

public interface UserService {
    List<User> selectAll();

    User selectById(long id);

    void save(User user);

    void deleteById(long id);

    int count();

    void update(User user);
    void updatePassword(User user);
}

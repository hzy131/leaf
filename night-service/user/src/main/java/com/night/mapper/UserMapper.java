package com.night.mapper;

import com.night.entity.User;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserMapper {
    List<User> selectAll();

    User selectById(long id);

    void save(User user);

    void deleteById(long id);

    int count();

    void update(User user);
    void updatePassword(User user);
}

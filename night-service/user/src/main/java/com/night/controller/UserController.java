package com.night.controller;

import com.night.entity.User;
import com.night.entity.UserVo;
import com.night.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/findAll")
    public UserVo findAll() {
        UserVo userVo = new UserVo();
        userVo.setCode(0);
        userVo.setCount(userService.count());
        userVo.setData(userService.selectAll());
        return userVo;
    }

    @GetMapping("/findById/{id}")
    public User findById(@PathVariable("id") long id) {
        return userService.selectById(id);
    }

    @PostMapping("/save")
    public void save(@RequestBody User user) {
        userService.save(user);
    }

    @PostMapping("/updatePassword")
    public void updatePassword(@RequestBody User user) {
        userService.updatePassword(user);
    }
    @PostMapping("/update")
    public void update(@RequestBody User user) {
        userService.update(user);
    }

    @GetMapping("/deleteById/{id}")
    public void deleteById(@PathVariable("id") long id) {
        userService.deleteById(id);
    }

    @RequestMapping("/list")
    public String list(@RequestBody User user){

        System.out.println(user.getId());
        return "123";
    }

}

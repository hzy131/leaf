package com.night.service;

import com.night.entity.Type;

import java.util.List;

public interface TypeService {
    List<Type> selectAll();
}

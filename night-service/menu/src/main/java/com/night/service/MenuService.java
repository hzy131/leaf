package com.night.service;


import com.night.entity.Menu;

import java.util.List;


public interface MenuService {
    List<Menu> selectAll(int index, int limit);

    Menu selectById(Long id);

    int count();

    void save(Menu menu);

    void update(Menu menu);

    void deleteById(long id);
}

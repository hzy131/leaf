package com.night.service.imp;

import com.night.entity.Type;
import com.night.mapper.TypeMapper;
import com.night.service.TypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TypeServiceImp implements TypeService {
    @Autowired
    private TypeMapper typeMapper;

    @Override
    public List<Type> selectAll() {
        return typeMapper.selectAll();
    }
}

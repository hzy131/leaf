package com.night.service.imp;

import com.night.entity.Menu;
import com.night.mapper.MenuMapper;
import com.night.service.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MenuServiceImp implements MenuService {
    @Autowired
    private MenuMapper menuService;

    @Override
    public List<Menu> selectAll(int index, int limit) {
        return menuService.selectAll(index, limit);
    }

    @Override
    public Menu selectById(Long id) {
        return menuService.selectById(id);
    }

    @Override
    public int count() {
        return menuService.count();
    }

    @Override
    public void save(Menu menu) {
        menuService.save(menu);

    }


    @Override
    public void update(Menu menu) {
        menuService.update(menu);

    }

    @Override
    public void deleteById(long id) {
        menuService.deleteById(id);

    }
}

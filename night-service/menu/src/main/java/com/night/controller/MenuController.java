package com.night.controller;

import com.night.entity.Menu;
import com.night.entity.MenuVo;
import com.night.entity.Type;
import com.night.service.MenuService;
import com.night.service.TypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/menu")
public class MenuController {

    @Autowired
    private MenuService menuService;

    @Autowired
    private TypeService typeService;

    //菜的类型
    @GetMapping("/typeFindAll")
    public List<Type> TypeFindAll() {

        return typeService.selectAll();
    }

    //查询出所有
    @GetMapping("/findAll/{page}/{limit}")
    public MenuVo findAll(@PathVariable("page") int page, @PathVariable("limit") int limit) {
        MenuVo menuVo = new MenuVo();
        menuVo.setCode(0);
        menuVo.setCount(menuService.count());
        menuVo.setData(menuService.selectAll((page - 1) * limit, limit));
        return menuVo;
    }

    //根据id查询
    @GetMapping("/findById/{id}")
    public Menu findById(@PathVariable("id") Long id) {
        return menuService.selectById(id);
    }

    //删除
    @GetMapping("/deleteById/{id}")
    public void deleteById(@PathVariable("id") Long id) {
        menuService.deleteById(id);
    }

    //保存
    @PostMapping("/save")
    public void save(@RequestBody Menu menu) {
        menuService.save(menu);
    }

    //修改
    @PostMapping("/update")
    public void update(@RequestBody Menu menu) {
        menuService.update(menu);
    }
}

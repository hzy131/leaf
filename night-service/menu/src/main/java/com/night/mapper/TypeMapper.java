package com.night.mapper;


import com.night.entity.Type;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TypeMapper {
    List<Type> selectAll();
}

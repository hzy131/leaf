package com.night.mapper;

import com.night.entity.Menu;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MenuMapper {
    List<Menu> selectAll(int index, int limit);

    int count();

    void save(Menu menu);

    Menu selectById(long id);

    void update(Menu menu);

    void deleteById(long id);
}

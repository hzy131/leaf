package com.night.service.Imp;

import com.night.entity.Order;
import com.night.mapper.OrderMapper;
import com.night.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderServiceImp implements OrderService {
    @Autowired
    private OrderMapper orderMapper;

    @Override
    public void save(Order order) {
        orderMapper.save(order);
    }

    @Override
    public List<Order> selectAllByUid(int index, int limit, int state) {
        return orderMapper.selectAllByUid(index, limit, state);
    }

    @Override
    public int countByUid(long uid) {
        return orderMapper.countByUid(uid);
    }

    @Override
    public void deleteByMid(long mid) {

    }

    @Override
    public void deleteByUid(long uid) {

    }

    @Override
    public List<Order> findAllByState(int state, int index, int limit) {
        return
                orderMapper.findAllByState(state, index, limit);
    }

    @Override
    public int countByState(int state) {
        return orderMapper.countByState(state);
    }

    @Override
    public void updateState(long id) {
        orderMapper.updateState(id);

    }
}
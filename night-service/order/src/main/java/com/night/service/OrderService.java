package com.night.service;

import com.night.entity.Order;

import java.util.List;

public interface OrderService {
    public void save(Order order);

    public List<Order> selectAllByUid(int index, int limit, int state);

    public int countByUid(long uid);

    public void deleteByMid(long mid);

    public void deleteByUid(long uid);

    public List<Order> findAllByState(int state, int index, int limit);

    public int countByState(int state);

    public void updateState(long id);
}

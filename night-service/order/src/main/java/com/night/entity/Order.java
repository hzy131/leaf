package com.night.entity;

import lombok.Data;

import java.util.Date;

@Data
public class Order {
    private int id;
    private User user;
    private Menu menu;
    private Date date;
    private int state;
}

package com.night.mapper;

import com.night.entity.User;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserMapper {
    List<User> selectAll();

    User selectById(Long id);

    User save(User user);

    void deleteById(Long id);

    int count();

    User update(User user);
}

package com.night.mapper;

import com.night.entity.Order;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderMapper {
    public void save(Order order);

    List<Order> selectAllByUid(int index, int limit, int state);

    public int countByUid(long uid);

    public void deleteByMid(long mid);

    public void deleteByUid(long uid);

    public List<Order> findAllByState(int state, int index, int limit);

    public int countByState(int state);

    public void updateState(long id);
}

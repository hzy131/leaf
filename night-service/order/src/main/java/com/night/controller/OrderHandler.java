package com.night.controller;


import com.night.entity.Order;
import com.night.entity.OrderVO;
import com.night.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/order")
public class OrderHandler {
    @Autowired
    private OrderService orderService;

    @PostMapping("/save")
    public void save(@RequestBody Order order) {
        orderService.save(order);
    }

    @GetMapping("/findAll/{page}/{limit}/{state}")
    public OrderVO findAllByUid(@PathVariable("page") int page, @PathVariable("limit") int limit, @PathVariable("state") int state) {
        OrderVO orderVO = new OrderVO();
        orderVO.setCode(0);
        Long uid = 1L;
        orderVO.setCount(orderService.countByUid(uid));
        orderVO.setData(orderService.selectAllByUid((page - 1) * limit, limit, state));
        return orderVO;
    }

    @GetMapping("/countByUid/{uid}")
    public int countByUid(@PathVariable("uid") int uid) {
        return orderService.countByUid(uid);
    }

    @DeleteMapping("/deleteByMid/{mid}")
    public void deleteByMid(@PathVariable("mid") long mid) {
        orderService.deleteByMid(mid);
    }

    @DeleteMapping("/deleteByUid/{uid}")
    public void deleteByUid(@PathVariable("uid") long uid) {
        orderService.deleteByUid(uid);
    }

    //
    @GetMapping("/findAllByState/{state}/{page}/{limit}")
    public OrderVO findAllByState(@PathVariable("state") int state, @PathVariable("page") int page, @PathVariable("limit") int limit) {
        OrderVO orderVO = new OrderVO();
        orderVO.setCode(0);
        orderVO.setMsg("");
        orderVO.setCount(orderService.countByState(0));
        orderVO.setData(orderService.findAllByState(0, (page - 1) * limit, limit));
        return orderVO;
    }

    //
    @RequestMapping("/updateState/{id}")
    public void updateState(@PathVariable("id") long id) {
        orderService.updateState(id);
    }
}

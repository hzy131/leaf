package com.night.controller;

import com.night.entity.Role;
import com.night.entity.User;
import com.night.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author leaf
 */
@RestController
@RequestMapping("/account")
public class AccountController {

    @Autowired
    private AccountService accountService;

    //登入验证
    @GetMapping("/login/{username}/{password}")
    public User login(@PathVariable("username") String username, @PathVariable("password") String password) {
        return accountService.login(username, password);
    }

    //查询用户的个人资料
    @GetMapping("/findByUserName/{username}")
    public User findByUserName(@PathVariable("username") String username) {
        return accountService.selectByUserName(username);
    }

    //查询用户的权限信息
    @GetMapping("/findByRoleName/{username}")
    public List<Role> findByRoleName(@PathVariable("username") String username) {
        System.out.println("查询用户权限" + username);
        return accountService.selectRoleByUsername(username);
    }

    //查询出所有的权限
    @GetMapping("/findRoleAll")
    public List<Role> findRoleAll() {
        return accountService.selectRoleAll();
    }
}

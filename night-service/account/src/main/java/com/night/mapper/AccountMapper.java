package com.night.mapper;

import com.night.entity.Role;
import com.night.entity.User;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AccountMapper {
    User login(String username, String password);

    User selectByUserName(String username);

    List<Role> selectRoleByUsername(String username);

    List<Role> selectRoleAll();

}
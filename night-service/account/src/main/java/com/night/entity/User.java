package com.night.entity;

import lombok.Data;

import java.util.Date;
import java.util.List;


@Data
public class User {
    private long id;
    private String username;
    private String password;
    private String nickName;
    private String gender;
    private String telephone;
    private Date registerDate;
    private String address;
    private String administrators;
    private List<Role> role;
}

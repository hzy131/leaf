package com.night.service;

import com.night.entity.Role;
import com.night.entity.User;

import java.util.List;

public interface AccountService {
    User login(String username, String password);

    User selectByUserName(String username);

    List<Role> selectRoleByUsername(String username);

    List<Role> selectRoleAll();
}

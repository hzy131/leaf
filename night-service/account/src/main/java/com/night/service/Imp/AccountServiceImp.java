package com.night.service.Imp;


import com.night.entity.Role;
import com.night.entity.User;
import com.night.mapper.AccountMapper;
import com.night.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AccountServiceImp implements AccountService {

    @Autowired
    private AccountMapper accountMapper;

    @Override
    public User login(String username, String password) {
        return accountMapper.login(username, password);
    }

    @Override
    public User selectByUserName(String username) {
        return accountMapper.selectByUserName(username);
    }

    @Override
    public List<Role> selectRoleByUsername(String username) {
        return accountMapper.selectRoleByUsername(username);
    }

    @Override
    public List<Role> selectRoleAll() {
        return accountMapper.selectRoleAll();
    }
}
